## TODO

- Improve partition prompt to include details
- Add an option to not wipe home
- Allow mounting other drives to automate genfstab

#!/bin/bash

efibootmgr -b 0 -B
efibootmgr -b 1 -B
efibootmgr -b 2 -B
efibootmgr -b 3 -B
efibootmgr -b 4 -B
UUID=$(blkid -s UUID -o value /dev/nvme0n1p2)
efibootmgr --create --disk /dev/nvme0n1 --part 1 --label "Arch Linux" --loader /vmlinuz-linux --unicode "root=UUID=$UUID rw loglevel=3 initrd=\initramfs-linux.img"

#!/bin/bash

# loadkeys fi
# cd /run
# tmux
# Ctrl + b, %
# Ctrl + b, "
# mount /dev/sda3 mount
# mount -t virtiofs share mount
# tail -f mount/log
# cd mount
# ./install.sh

source DEFAULTS

set -e

# if false
# then

systemctl start gpm

read -p "Connect to wifi? [y/N] " WIFICONNECT
case $WIFICONNECT in
  [yY])
    ./connectwifi.sh
  ;;
esac

set +e
efibootmgr &> /dev/null
case $? in
  0)
    FIRMWARE=UEFI
  ;;
  *)
    FIRMWARE=BIOS
  ;;
esac
set -e

CPU=$(awk -F': ' '/^vendor_id/ {print $2; exit}' /proc/cpuinfo)
case $CPU in
  AuthenticAMD);;
  GenuineIntel);;
  *)
    PS3="Select CPU vendor> "
    select CPU in AuthenticAMD GenuineIntel VM/Other
    do
      [ -n "${CPU}" ] && break
    done
  ;;
esac

if [ -z $NVIDIA ]
then
  read -p "Nvidia? [y/N] " nvidia
  case $nvidia in
    [yY])
      NVIDIA="true"
    ;;
    *)
      NVIDIA="false"
    ;;
  esac
fi

if [ -z $INSTALL ]
then
  PS3="Select install type> "
  select INSTALL in Full Quick Server
  do
    [ -n "${INSTALL}" ] && break
  done
fi

if [ -z $USERNAME ]
then
  read -p "Username: " USERNAME
fi
if [ -z $USER_PASSWORD ]
then
  read -p "User password: " USER_PASSWORD
fi
if [ -z $ROOT_PASSWORD ]
then
  read -p "Root password: " ROOT_PASSWORD
fi
if [ -z $HOSTNAME ]
then
  read -p "Hostname: " HOSTNAME
fi
if [ -z $REPO ]
then
  read -p "IP of local repo: " REPO
fi

echo
echo "Firmware: $FIRMWARE"
echo "Username: $USERNAME"
echo "User password: $USER_PASSWORD"
echo "Root password: $ROOT_PASSWORD"
echo "Hostname: $HOSTNAME"
echo "CPU vendor: $CPU"
echo "Nvidia: $NVIDIA"
echo "Install type: $INSTALL"
echo "Repo IP: $REPO"
echo -n "Press enter to continue..."
read

set +e
echo "UNMOUNTING PARTITIONS JUST IN CASE" | tee log
swapoff /mnt/swapfile
umount -R /mnt
set -e

lsblk

PS3="Select install drive> "
select DRIVE in $(lsblk -o name -lpnd);
do
  [ -n "${DRIVE}" ] && break
done

if [ -z $PARTITIONING ]
then
  read -p "Partition your drive? [y/N] " PARTITIONING
  case $PARTITIONING in
    [yY])
      case $FIRMWARE in
        UEFI)
          parted $DRIVE --script mklabel gpt mkpart primary fat32 0% 500MiB mkpart primary 500MiB 100%
        ;;
        BIOS)
          parted $DRIVE --script mklabel msdos mkpart primary 0% 100%
        ;;
      esac
      # cfdisk --zero $DRIVE
      # boot >500MB
      # root >100GB
    ;;
  esac
fi

# if [ -z $SEPARATEHOME ]
# then
#   read -p "Create a separate home partition? [y/N] " separatehome
#   case $separatehome in
#     [yY])
#       SEPARATEHOME="true"
#     ;;
#     *)
#       SEPARATEHOME="false"
#     ;;
#   esac
# fi

if [ $FIRMWARE = UEFI ]
then
  PS3="Select EFI partition> "
  select EFIPARTITION in $(grep -vx $DRIVE <(grep $DRIVE <(lsblk -o name -lpn)));
  do
    [ -n "${EFIPARTITION}" ] && break
  done
fi

PS3="Select root partition> "
select ROOTPARTITION in $(grep -vx $DRIVE <(grep $DRIVE <(lsblk -o name -lpn)));
do
  [ -n "${ROOTPARTITION}" ] && break
done

if [ $SEPARATEHOME = true ]
then
  PS3="Select home partition> "
  select HOMEPARTITION in $(grep -vx $DRIVE <(grep $DRIVE <(lsblk -o name -lpn)));
  do
    [ -n "${HOMEPARTITION}" ] && break
  done
fi

echo
echo "Install drive: $DRIVE"
echo "Boot partition: $EFIPARTITION"
echo "Root partition: $ROOTPARTITION"
echo "Home partition: $HOMEPARTITION"
echo -n "Press enter to continue..."
read

SYSTEM="base linux linux-firmware linux-headers nano grub efibootmgr sudo" #system stuff
# Automate check of cpu vendor for ucode?
case $CPU in
  AuthenticAMD)
    SYSTEM+=" amd-ucode"
  ;;
  GenuineIntel)
    SYSTEM+=" intel-ucode"
  ;;
esac

PACKAGES=""

case $INSTALL in
  Server)
    #For server installations
    PACKAGES+="openssh networkmanager kodi git base-devel bash-completion cage htop tealdeer man etckeeper apache qbittorrent-nox tmux opendoas btrfs-progs rust"
  ;;
  Quick)
    # For quick install tests
    PACKAGES+="plasma-meta"
  ;;
  Full)
    PACKAGES+="gnome-calculator kcalc kcolorchooser tmux qbittorrent asciinema hdparm kdenlive kalzium yakuake man tealdeer kdeconnect sshfs kdenetwork-filesharing kfind lutris wget wacomtablet pkgstats xsel kjumpingcube cmatrix keepassxc jre-openjdk gwenview kolourpaint kruler skanlite svgpart fcitx5-anthy pacman-contrib etckeeper aisleriot puzzles mold flatpak fwupd speedtest-cli blender kleopatra keysmith usb_modeswitch kdiskmark usbip inxi pipewire-v4l2 gst-plugin-pipewire kio-admin bash-completion nvtop corectrl vulkan-radeon lib32-vulkan-radeon libva-mesa-driver gstreamer-vaapi kwalletmanager plasma-sdk linux-firmware-qlogic krdc opencl-rusticl-mesa freecad syncthing firejail timeshift neochat neovim strace inetutils android-tools cmake power-profiles-daemon akregator songrec" #user programs

    PACKAGES+=" steam wine-staging wine-gecko wine-mono" #32-bit programs
    PACKAGES+=" cups system-config-printer gutenprint" #printing
    PACKAGES+=" otf-ipafont adobe-source-han-sans-kr-fonts adobe-source-han-sans-cn-fonts kcharselect" #fonts
    PACKAGES+=" virt-manager qemu dnsmasq" #virtual machines
    PACKAGES+=" dolphin-emu dosbox" #emulators
    PACKAGES+=" plasma-meta vulkan-devel base-devel kde-system fcitx5-im" #package groups
    PACKAGES+=" filelight xorg-xev glances htop tree" #system monitors
    PACKAGES+=" discord signal-desktop konversation telegram-desktop" #chat applications
    PACKAGES+=" arp-scan traceroute wol wireguard-tools dnsutils wireshark-qt globalprotect-openconnect nmap" #network
    PACKAGES+=" ark unrar" #packaging
    PACKAGES+=" obs-studio yt-dlp spectacle kamoso" #recording
    PACKAGES+=" ffmpegthumbs libappimage icoutils kdegraphics-thumbnailers qt6-imageformats libgsf" #thumbnailing
    PACKAGES+=" pipewire pipewire-pulse helvum wireplumber easyeffects sox qpwgraph" #audio
    PACKAGES+=" devtools vulkan-tools kirigami-gallery rust" #devtools
    PACKAGES+=" mpv okular mediainfo" #media viewing
    PACKAGES+=" code kate okteta gimp krita krita-plugin-gmic libreoffice-still" #editors
    PACKAGES+=" firefox firefox-ublock-origin" #browser
    PACKAGES+=" btrfs-progs mtools bcachefs-tools f2fs-tools" #filesystems
  ;;
esac
if [ $NVIDIA = true ]
then
  PACKAGES+=" nvidia nvidia-dkms lib32-nvidia-utils nvidia-settings"
fi

# Dependencies
# imagemagick openssh plasma-browser-integration print-manager

# Unneeded?
# pandoc
# mkvtoolnix-gui
# chromium
# dhcpcd
# lib32-vulkan-icd-loader
# pcsx2

echo "FORMATTING PARTITIONS" | tee -a log
if [ $FIRMWARE = UEFI ]
then
  mkfs.fat -n boot  $EFIPARTITION
fi
mkfs.btrfs -f -L root $ROOTPARTITION
if [[ $HOMEPARTITION ]]
then
  mkfs.btrfs -f -L home $HOMEPARTITION
fi

echo "MOUNTING PARTITIONS" | tee -a log
mount $ROOTPARTITION /mnt
if [ $FIRMWARE = UEFI ]
then
  mkdir /mnt/boot
  mount $EFIPARTITION /mnt/boot
fi
if [[ $HOMEPARTITION ]]
then
  mkdir /mnt/home
  mount $HOMEPARTITION /mnt/home
fi

echo "SETTING UP SWAPFILE" | tee -a log
btrfs filesystem mkswapfile --size 8g --uuid clear /mnt/swapfile
swapon /mnt/swapfile

set +e
echo "INSTALLING BASE LINUX" | tee -a log
until pacstrap -K /mnt $SYSTEM
do
  sleep 1
done
set -e

echo "GENERATING FSTAB" | tee -a log
genfstab -U /mnt >> /mnt/etc/fstab

echo "SETTING LOCALTIME" | tee -a log
arch-chroot /mnt ln -sf /usr/share/zoneinfo/Europe/Helsinki /etc/localtime

echo "SETTING HWCLOCK" | tee -a log
arch-chroot /mnt hwclock --systohc

echo "UNCOMMENTING LOCALE" | tee -a log
LOCGEN="/etc/locale.gen"
arch-chroot /mnt sed -i "s/#en_GB.U/en_GB.U/" $LOCGEN
arch-chroot /mnt sed -i "s/#en_US.U/en_US.U/" $LOCGEN
arch-chroot /mnt sed -i "s/#fi_FI.U/fi_FI.U/" $LOCGEN
arch-chroot /mnt sed -i "s/#ja_JP.U/ja_JP.U/" $LOCGEN

echo "GENERATING LOCALE" | tee -a log
arch-chroot /mnt locale-gen

echo "SETTING LANGUAGE" | tee -a log
echo "LANG=en_GB.UTF-8" > /mnt/etc/locale.conf
echo "LANGUAGE=en_US" >> /mnt/etc/locale.conf

echo "SETTING KEYMAP" | tee -a log
echo "KEYMAP=fi" > /mnt/etc/vconsole.conf

echo "SETTING HOSTNAME" | tee -a log
echo "$HOSTNAME" > /mnt/etc/hostname

echo "SETTING ROOT PASSWORD" | tee -a log
printf "$ROOT_PASSWORD\n$ROOT_PASSWORD" | arch-chroot /mnt passwd

echo "ENABLING MULTILIB" | tee -a log
arch-chroot /mnt sed -i "/\[multilib\]/,/Include/ s/^#//" /etc/pacman.conf

echo "SETTING PACMAN COLORS AND ENABLING PARALLEL DOWNLOADS" | tee -a log
arch-chroot /mnt sed -i 's/^#Color/Color/' /etc/pacman.conf
arch-chroot /mnt sed -i 's/^#ParallelDownloads/ParallelDownloads/' /etc/pacman.conf

set +e
echo "INSTALLING PACKAGES" | tee -a log
until arch-chroot /mnt pacman -Suy --noconfirm $PACKAGES
do
  sleep 1
done
set -e

case $FIRMWARE in
  UEFI)
    echo "SETTING EFI STUB" | tee -a log
    set +e
    efibootmgr -b 0 -B
    efibootmgr -b 1 -B
    efibootmgr -b 2 -B
    efibootmgr -b 3 -B
    efibootmgr -b 4 -B
    set -e
    UUID=$(blkid -s UUID -o value $ROOTPARTITION)
    efibootmgr --create --disk $DRIVE --part 1 --label "Arch Linux" --loader /vmlinuz-linux --unicode "root=UUID=$UUID rw loglevel=3 initrd=\initramfs-linux.img"
  ;;
  BIOS)
    echo "INSTALLING GRUB" | tee -a log
    arch-chroot /mnt grub-install --target=i386-pc $DRIVE
    arch-chroot /mnt sed -i "s/GRUB_TIMEOUT=5/GRUB_TIMEOUT=1/" /etc/default/grub
    arch-chroot /mnt sed -i "s/loglevel=3 quiet/loglevel=3/" /etc/default/grub
    arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
  ;;
esac

echo "ADDING USER" | tee -a log
arch-chroot /mnt useradd --create-home $USERNAME

echo "GENERATING SUDO GROUP"
arch-chroot /mnt groupadd sudo

echo "ADDING USER TO SUDO AND WHEEL GROUP" | tee -a log
arch-chroot /mnt usermod -aG sudo,wheel $USERNAME

echo "GIVING SUDO GROUP SUDO PRIVILEGES" | tee -a log
arch-chroot /mnt sed -i "s/# %sudo/%sudo/" /etc/sudoers
# arch-chroot /mnt sed -i "s/# %wheel/%wheel/" /etc/sudoers

echo "GIVING DOAS PRIVILEGES" | tee -a  log
echo "permit persist $USERNAME as root" > /mnt/etc/doas.conf

echo "SETTING USER PASSWORD" | tee -a log
printf "$USER_PASSWORD\n$USER_PASSWORD" | arch-chroot /mnt passwd $USERNAME

echo "ENABLING NETWORKMANAGER" | tee -a log
arch-chroot /mnt systemctl enable NetworkManager.service

echo "ENABLING TIMESYNCD" | tee -a log
arch-chroot /mnt systemctl enable systemd-timesyncd.service

case $INSTALL in
  Full|Server)
    echo "ENABLING SSHD" | tee -a log
    arch-chroot /mnt systemctl enable sshd.service
  ;;&
  Full|Quick)
    echo "ENABLING SDDM" | tee -a log
    arch-chroot /mnt systemctl enable sddm.service

    echo "ENABLING BLUETOOTH" | tee -a log
    arch-chroot /mnt systemctl enable bluetooth.service
  ;;&
  Full)
    echo "ENABLING LIBVIRT" | tee -a log
    arch-chroot /mnt systemctl enable libvirtd.service
    arch-chroot /mnt usermod -aG libvirt $USERNAME


    echo "ENABLING CUPS" | tee -a log
    arch-chroot /mnt systemctl enable cups.service

    echo "ENABLING PACCACHE CLEANER" | tee -a log
    arch-chroot /mnt systemctl enable paccache.timer
  ;;
esac

echo "COPYING SYSTEM SETTINGS" | tee -a log
cp -R etc/. /mnt/etc/

echo "COPYING USERCONFIGS" | tee -a log
cp -R userconfigs/. /mnt/home/$USERNAME/

echo "SETTING USERCONFIG OWNERSHIP" | tee -a log
arch-chroot /mnt chown -R $USERNAME:$USERNAME /home/$USERNAME/

echo "ACTIVATING OMA REPO" | tee -a log
echo "[oma]" >> /mnt/etc/pacman.conf
echo "SigLevel = Optional TrustAll" >> /mnt/etc/pacman.conf
echo "Server = http://$REPO" >> /mnt/etc/pacman.conf

set +e
echo "INSTALLING FROM OMA" | tee -a log
AURPACKAGES="waydroid kwin-scripts-krohnkite-git kwin-effects-burn-my-windows-git ttf-twemoji ttf-ms-fonts czkawka-gui brother-hll2350dw yay vesktop-electron mission-center ladybird klassy kde-thumbnailer-apk informant upd72020x-fw ast-firmware aic94xx-firmware wd719x-firmware waypipe f3"
until arch-chroot /mnt pacman -Suy --noconfirm $AURPACKAGES
do
  sleep 1
done
set -e

echo "LINKING TWEMOJI" | tee -a log
arch-chroot /mnt ln -sf /usr/share/fontconfig/conf.avail/75-twemoji.conf /etc/fonts/conf.d/75-twemoji.conf

if [ $INSTALL = Full ]
then
  echo "ADDITIONAL TWEAKS" | tee -a log
  arch-chroot /mnt cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak
  arch-chroot /mnt rankmirrors /etc/pacman.d/mirrorlist.bak > /etc/pacman.d/mirrorlist
  arch-chroot /mnt git config --global user.email "root"
  arch-chroot /mnt git config --global user.name "root"
  arch-chroot /mnt etckeeper init
  arch-chroot /mnt ln -s /dev/null /etc/systemd/network/99-default.link
fi

echo "FINISHED" | tee -a log

# aurutils
# aurutils kodi-standalone-service steamcmd
# activate-linux android-studio grub-theme-minegrub minegrub-theme-update-service postman-bin ruffle-nightly-bin showmethekey unityhub firefox-extension-plasma-integration-bin minecraft-launcher bruno/bruno-bin

# Plasma plugins
# krohnkite https://store.kde.org/p/2144146
# TV Effect https://store.kde.org/p/1884312
# Klassy https://store.kde.org/p/1877255/

# flatpak
# soundux

# Browser extentions
#
# Clickbait remover
# Old reddit redirect
# Return Youtube dislike
# Search by image
# Sponsorblock
# Startpage
# ytcFilter
# Youtube-shorts block
# Plasma Integration
# CleanURLs
# I still don't care about cookies
# Unhook

# Maybe?
# Web Developer
# Measure-it
# Save webP as PNG or JPEG
# Personal Blocklist
# Allow Right-Click
# NoScript
# Archive page
# Ground News
# Rikaikun
# De-Mainstream Youtube
# dearrow to replace clickbait remover

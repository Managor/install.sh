alias scanwifi='nmcli dev wifi'
alias bootuefi='systemctl reboot --firmware-setup'
alias getip='curl ifconfig.me;echo'
alias winek='killall -2 -v wineserver'
alias refresh='source ~/.bashrc'
alias compile='gcc main.c -lpthread && ./a.out'
alias pömpeli='ssh serveri'
alias niisku='ssh savoant3@niisku.lab.fi'
alias choose='shopt -s globstar; shuf -n1 -e **/*'
alias df='df -h'
#alias mbcachefs="sudo bcachefs mount UUID=7f25a0b9-cbb8-4b03-ad55-5a311e3856e7 /mnt/bcachefs"
alias mbcachefs="sudo bcachefs mount /dev/nvme1n1:/dev/sdc /mnt/bcachefs/"
alias steamconsole="steam steam://open/console"
alias upandshut="sudo pacman -Suy --noconfirm && qdbus6 org.kde.Shutdown /Shutdown logoutAndShutdown"
alias diskmount="udisksctl mount -b"
alias diskumount="udisksctl unmount -b"
alias micloop="pw-link alsa_input.usb-046d_HD_Pro_Webcam_C920_A19DECCF-02.analog-stereo:capture_FL alsa_output.usb-ASUS_TUF_H3_Wireless-00.analog-stereo:playback_FL; pw-link alsa_input.usb-046d_HD_Pro_Webcam_C920_A19DECCF-02.analog-stereo:capture_FR alsa_output.usb-ASUS_TUF_H3_Wireless-00.analog-stereo:playback_FR"
alias micloop2="pw-link alsa_input.usb-ASUS_TUF_H3_Wireless-00.mono-fallback:capture_MONO alsa_output.usb-ASUS_TUF_H3_Wireless-00.analog-stereo:playback_FL; pw-link alsa_input.usb-ASUS_TUF_H3_Wireless-00.mono-fallback:capture_MONO alsa_output.usb-ASUS_TUF_H3_Wireless-00.analog-stereo:playback_FR"
alias hearnomore="pw-link -d alsa_input.usb-046d_HD_Pro_Webcam_C920_A19DECCF-02.analog-stereo:capture_FL alsa_output.usb-ASUS_TUF_H3_Wireless-00.analog-stereo:playback_FL; pw-link -d alsa_input.usb-046d_HD_Pro_Webcam_C920_A19DECCF-02.analog-stereo:capture_FR alsa_output.usb-ASUS_TUF_H3_Wireless-00.analog-stereo:playback_FR; pw-link -d alsa_input.usb-ASUS_TUF_H3_Wireless-00.mono-fallback:capture_MONO alsa_output.usb-ASUS_TUF_H3_Wireless-00.analog-stereo:playback_FL; pw-link -d alsa_input.usb-ASUS_TUF_H3_Wireless-00.mono-fallback:capture_MONO alsa_output.usb-ASUS_TUF_H3_Wireless-00.analog-stereo:playback_FR"
alias mousereset="sudo usb_modeswitch -v 0x22d4 -p 0x1803 --reset-usb"
alias etätuki="ssh anja@10.0.1.10"
alias fetchpkg="pkgctl repo clone --protocol=https"
alias purgecoredumps="sudo journalctl --vacuum-time=2d"
alias kreboot="qdbus6 org.kde.Shutdown /Shutdown logoutAndReboot"
alias kshutdown="qdbus6 org.kde.Shutdown /Shutdown logoutAndShutdown"
alias klogout="qdbus6 org.kde.Shutdown /Shutdown logout"
alias purgeorphans="pacman -Qtdq | sudo pacman -Rns -"
alias fixdesktop="systemctl --user restart plasma-plasmashell"
alias checkram="sudo dmidecode --type 17"
alias checkdisks="sudo fdisk -l"
alias mountshare="sudo mount -t 9p -o trans=virtio /Public ~/Public"
alias refreshkeys="sudo pacman -S archlinux-keyring"
alias putty="GDK_BACKEND=x11 putty"
alias kb="kde-builder"
alias wake2="wol 04:92:26:d4:b6:4f"
alias venv="python -m venv .venv && source .venv/bin/activate"
alias sysstart="sudo systemctl start"

#ssh-keygen
#ssh-copy-id [remote location]


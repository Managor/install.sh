#
# ~/.bashrc
#

export HISTCONTROL="ignoreboth"

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1="[\[\e[90m\]\A\[\e[0m\]][\[\e[32m\]\u@\h\[\e[0m\] [\[\e[94m\]\$?\[\e[0m\]]\W]$ "
PS2="\[\e[34;3m\]multiline-> \[\e[0m\]"
export PS3=$'\[\e[92;1m\]Choose an option: \[\e[0m\]'
export PS4='>\[\e[101;30m\]DEBUG:${BASH_SOURCE:+$BASH_SOURCE:}${FUNCNAME:+$FUNCNAME():}$LINENO:\[\e[0m\] '

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share
export EDITOR=nano
export MAKEFLAGS="-j$(nproc)"
export RUSTICL_ENABLE=radeonsi
export BROWSER=firefox

export PATH="$HOME/.local/bin:$PATH"

if [ -d ~/.local/share/bash_completion/completions/ ]; then
    for file in ~/.local/share/bash_completion/completions/*; do
        [ -f "$file" ] && . "$file"
    done
fi

set -o vi
